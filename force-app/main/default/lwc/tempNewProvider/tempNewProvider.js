import { LightningElement, track } from 'lwc';

export default class TempNewProvider extends LightningElement {
    ReadOnlyPhoneType = true;
    @track PhoneNumber = '';
    @track Extension = '';
    @track PhoneType = 'Medical Records Contact records';

    handleChange(event) {
        const field = event.target.name;
        if (field === 'PhoneNumber') {
            this.firstName = event.target.value;
        } else if (field === 'Extension') {
            this.lastName = event.target.value;
        }else if (field === 'PhoneType') {
            this.lastName = event.target.value;
        }
    }

}